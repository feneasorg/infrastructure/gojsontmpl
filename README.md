# GO Json Template Parser

Is a simple script to parse html templates.

## Compile

    go build -o dist/gojsontmpl .

## Usage

    ./gojsontmpl \
      -html-base-file test.tmpl \
      -html-files test.tmpl \
      -json-file test.json

You can specify multiple template files with `-html-files`.

### Templates

The templates are based on [html/template](https://golang.org/pkg/html/template/)!

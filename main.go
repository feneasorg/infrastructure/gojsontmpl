package main
//
// GO Json Template Parser
// Copyright (C) 2019 Lukas Matt <lukas@matt.wf>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

import (
  "html/template"
  "encoding/json"
  "flag"
  "os"
  "strings"
  "io/ioutil"
  "fmt"
  "time"
)

var leftDelim, rightDelim string
var jsonFilePath, htmlFilePath, htmlBase string

var funcMap = template.FuncMap{
  "date": func() string {
    return time.Now().Format("02.01.2006")
  },
  "raw": func(val string) template.HTML {
    return template.HTML(val)
  },
  "set": func(viewArgs map[string]interface{}, key string, value interface{}) template.JS {
    viewArgs[key] = value
    return template.JS("")
  },
  "toint": func(v interface{}) int {
    float, ok := v.(float64)
    if ok {
      return int(float)
    }
    return v.(int)
  },
  "tostring": func(v interface{}) string {
    return v.(string)
  },
}

func init() {
  flag.StringVar(&jsonFilePath, "json-file", "", "The json file you want to parse")
  flag.StringVar(&htmlFilePath, "html-files", "", "The html templates (separated by ',')")
  flag.StringVar(&htmlBase, "html-base-file", "index.html.tmpl", "The base name e.g index.html.tmpl")
  flag.StringVar(&leftDelim, "left-delim", "{{", "The left delimiter used in all go templates")
  flag.StringVar(&rightDelim, "right-delim", "}}", "The right delimiter used in all go templates")
}

func main() {
  flag.Parse()
  if jsonFilePath == "" || htmlFilePath == "" {
    fmt.Printf("\n  %s\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n\n",
      "Following helper functions are available:",
      "date\t\tWill return with the current date (dd.mm.yyyy)",
      "raw\t\tWill not escape HTML code",
      "set\t\tCan add variables to the current context",
      "toint\t\tConvert interface type to int",
      "tostring\tConvert interface type to string")
    flag.PrintDefaults()
    os.Exit(1)
  }

  jsonData, err := ioutil.ReadFile(jsonFilePath)
  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }

  jsonCtx := []map[string]interface{}{}
  err = json.Unmarshal(jsonData, &jsonCtx)
  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }

  htmlFilePaths := strings.Split(htmlFilePath, ",")
  tmpl := template.Must(template.New(htmlBase).
    Funcs(funcMap).Delims(leftDelim, rightDelim).ParseFiles(htmlFilePaths...))

  var ctx = map[string]interface{}{"JSON": jsonCtx}
  err = tmpl.Execute(os.Stdout, ctx)
  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }
}
